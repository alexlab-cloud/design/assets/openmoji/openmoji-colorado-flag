# Sources

- [`assets/colorado-flag-original.svg`][1]
- [Colorado state flag design specification][2]
- ["Flag for Colorado (US-CO)" emoji technical information][3]

[1]: https://en.m.wikipedia.org/wiki/File:Flag_of_Colorado.svg
[2]: https://usflags.design/colorado/
[3]: https://emojipedia.org/flag-for-colorado-usco#technical
