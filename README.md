# openmoji-colorado-flag

A prototype OpenMoji for the unicode character [`U+1F3F4 U+E0075 U+E0073 U+E0063 U+E006F U+E007F`, "Flag for Colorado (US-CO)".][1]

This project has been built and normalized according to the [OpenMoji contribution guide][2].

## Design

### Emojipedia Original

![Emojipedia example][emojipedia-example]

### OpenMoji Prototype

![OpenMoji prototype][openmoji-prototype]

<!-- Images -->

[emojipedia-example]: ./assets/readme/emojipedia-example.png
[openmoji-prototype]: ./assets/readme/openmoji-prototype.png

<!-- References -->

[1]: https://emojipedia.org/flag-for-colorado-usco#technical
[2]: https://github.com/hfg-gmuend/openmoji/blob/master/CONTRIBUTING.md
